import './main.css';

import Header from './components/header/Header';
import Landing from './components/landing/Landing';


import { useState } from "react";

import { Routes,Route } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { BrowserRouter  } from 'react-router-dom';

import { useEffect } from "react";
import { WavyContainer } from "react-wavy-transitions";


function App() {
  //const location = useLocation();
  return (
    
      <BrowserRouter >
        <WavyContainer/>
        <div className="App">
          <Header></Header>
            <Routes > 
                <Route exact  path="" element={<Landing />} />
            </Routes>
        </div>
      </BrowserRouter>
  );
}

export default App;
