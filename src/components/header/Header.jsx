import './header.css';
import { WavyLink } from 'react-wavy-transitions';

function Header() {
    return (
<header>
    <div className="name">
        Taiel
    </div>
      
    
    
    <nav id="header__nav">
        <WavyLink to="" className="item">
            <div className="num">01</div>
            <div className="item_name">// home</div>
        </WavyLink>
        <WavyLink to="/proyectos" className="item">
            <div className="num">02</div>
            <div className="item_name">// proyectos</div>
        </WavyLink>
        <WavyLink to="/experiencia" className="item">
            <div className="num">03</div>
            <div className="item_name">// experiencia</div>
        </WavyLink>
        <WavyLink to="/ahora" className="item">
            <div className="num">04</div>
            <div className="item_name">// ahora</div>
        </WavyLink>
        <WavyLink to="/contacto" className="item">
            <div className="num">05</div>
            <div className="item_name">// contacto</div>
        </WavyLink>
    </nav>
</header>
    );
  }
  
  export default Header;
  