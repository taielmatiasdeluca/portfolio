import './landing.css';

import bg from '../../static/images/black-bg.webp'

function Landing() {
    return (
        <div className="landing">
            <img src={bg} id="bg__image" />

        </div>
        
    );
  }
  
  export default Landing;
  